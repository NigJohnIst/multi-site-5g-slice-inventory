# Multi-site 5G slice inventory

Repository containing documentation and API defintion of the VITAL-5G Multi-site 5G slice inventory.  
The `OpenApi` can be found [here](https://studio.apicur.io/apis/81153/collaboration/accept/3b87a213-52c4-43cb-8f40-b8c2f04ec1ce).
The IP-addresses of the Slice inventory are: `172.28.18.101`(DEV) and `172.28.18.69`(PROD). (Both are running 24/7). 

## Southbound interfaces
Towards the local slice plugin on the testbed. 
### For the **URLLC** slice

1. To `add` a **URLLC** slice -> `POST` Request: 
- `172.28.18.101:8080/api/v1/sliceInventory/SB/reportSliceParameters/urllc`
`JSON` body:
```
{
  "sliceId": "AntwerpUrllc01",
  "site": "antwerp",
  "latency": 14,
  "jitter": 1,
  "expDataRateUL": 100,
  "expDataRateDL": 100,
  "trafficType": "TCP",
  "imsi":["imsi1", "imsi2", "imsi3"]
}
```
2. To `update` a **URLLC** slice -> `PUT` Request: 
- `172.28.18.101:8080/api/v1/sliceInventory/SB/reportSliceParameters/urllc`
`JSON` body:
```
{
  "sliceId": "AntwerpUrllc01",
  "site": "Antwerp",
  "latency": 16,
  "jitter": 1,
  "expDataRateUL": 100,
  "expDataRateDL": 100,
  "trafficType": "UDP",
  "imsi":["imsi1", "imsi2", "imsi3", "imsi4"]
}
```
3. To `delete` a **URLLC** slice -> `DELETE` Request: 
- `172.28.18.101:8080/api/v1/sliceInventory/SB/reportSliceParameters/urllc`
`JSON` body:
```
{
  "sliceId": "AntwerpUrllc01",
  "site": "Antwerp",
  "latency": 16,
  "jitter": 1,
  "expDataRateUL": 100,
  "expDataRateDL": 100,
  "trafficType": "UDP",
  "imsi":["imsi1", "imsi2", "imsi3", "imsi4"]
}
```

### For the **EMBB** slice
1. To `add` a **EMBB** slice -> `POST` Request: 
- `172.28.18.101:8080/api/v1/sliceInventory/SB/reportSliceParameters/embb`
`JSON` body:
```
{
  "sliceId": "AntwerpEmbb01",
  "site": "antwerp",
  "expDataRateUL": 200,
  "expDataRateDL": 200,
  "areaTrafficCapUL": 200,
  "areaTrafficCapDL": 200,
  "userDensity": 200,
  "userSpeed": 80,
  "trafficType": "TCP",
  "imsi":["imsi1", "imsi2", "imsi3"]
}
```
2. To `update` a **EMBB** slice -> `PUT` Request: 
- `172.28.18.101:8080/api/v1/sliceInventory/SB/reportSliceParameters/embb`
`JSON` body:
```
{
  "sliceId": "AntwerpEmbb01",
  "site": "Antwerp",
  "expDataRateUL": 400,
  "expDataRateDL": 200,
  "areaTrafficCapUL": 200,
  "areaTrafficCapDL": 200,
  "userDensity": 200,
  "userSpeed": 80,
  "trafficType": "UDP",
  "imsi":["imsi1", "imsi2", "imsi3", "imsi4"]
}
```
3. To `delete` a **EMBB** slice -> `DELETE` Request: 
- `172.28.18.101:8080/api/v1/sliceInventory/SB/reportSliceParameters/embb`
`JSON` body:
```
{
  "sliceId": "AntwerpEmbb01",
  "site": "Antwerp",
  "expDataRateUL": 400,
  "expDataRateDL": 200,
  "areaTrafficCapUL": 200,
  "areaTrafficCapDL": 200,
  "userDensity": 200,
  "userSpeed": 80,
  "trafficType": "UDP",
  "imsi":["imsi1", "imsi2", "imsi3", "imsi4"]
}
```

### For the dynamical creation of a slice
As agreed is not yet supported will be supported in upcoming releases (when all the local slice plugins supports it).

### To attach an experiment to a slice [REST call from Slice Inventory to Slice Plugin]
1. When a slice has been selected on the VITAL-5G Slice Inventory and the VITAL-5G Service LCM communicates to attach the slice then it will be communicated like this towards the local Slice Plugin on the corresponding testbed: `POST` request (thus to the Slice Plugin). 
- `REST endpoint on the local Slice Plugin on the local testbed/sliceUE/attach`
`JSON` body:
```
{
  "imsi": "TercofinII",
  "sliceId": "AntwerpEmbb01",
  "usedDataRateUL": 2,
  "usedDataRateDL": 20
}
```
HttpStatuscode `200` expected if everything is succesfull, when unsuccessful: HttpStatuscode `501` expected. 
If the VITAL-5G Slice Inventory receives HttpStatuscode `200` it will comunicate towards the Service LCM that the experiment is succesfully attached towards the was succesfull. 
